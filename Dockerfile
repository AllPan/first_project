# version 1.0.0
FROM nginx:alpine
MAINTAINER Oleksandr Panfilov
RUN echo "Changed start page and port on 8585" > /usr/share/nginx/html/index.html
COPY default.conf /etc/nginx/conf.d/
EXPOSE 8585